import styled from "styled-components";

const HeaderContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 4vw;
  top: 0px;
  right: 0px;

  font-family: IBM3270;
`;

const HeaderLinkContainer = styled.div`
  margin: 0px;
  position: absolute;
  height: 100%;
  top: 0px;
  right: 0px;

  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

const HeaderLink = styled.a`
  display: block;
  color: red;
  padding: 2vw;

  &:after {
    content: "";
    background: red;
    position: absolute;
    bottom: 25%;
    margin-left: calc(2vw - 1px);
    height: 50%;
    width: 1px;
    overflow-y: hidden;
  }
`;

const HeaderLogo = styled.div`
  position: absolute;
  height: 100%;
  top: 0px;
  left: 0px;
  padding: 0vw 2vw;

  display: flex;
  align-items: center;

  font-size: 3vw;
`;

export { HeaderContainer, HeaderLinkContainer, HeaderLink, HeaderLogo };
