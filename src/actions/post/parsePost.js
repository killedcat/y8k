const multipliers = [86400, 3600, 60, 1]; // day, hour, minute, second
const acronyms = ["d", "h", "m", "s"];
const corrupted = /<CORRUPTED ([0-9]+)>/g;

const corruptWithGenerator = (length, generator) =>
  Array.from({ length }, () =>
    String.fromCharCode(Math.floor(generator() % 8000))
  ).join("");

const parsePost = (post) => {
  // https://gist.github.com/blixt/f17b47c62508be59987b
  const LCG = (s) => () => (s = (Math.imul(48271, s) >>> 0) % 2147483647);
  const generator = LCG(post.time);

  const corruptedString = (min, max) =>
    corruptWithGenerator(
      Math.floor((generator() / 2147483647) * (max - min) + min),
      generator
    );

  post.replies = post.replies
    .map((reply) => {
      if (reply.corrupted) {
        return Array.from({ length: reply.corrupted }, () => ({
          author: corruptedString(5, 20),
          time: corruptedString(14, 14),
          readableTime: corruptedString(4, 10),
          content: corruptedString(10, 100),
        }));
      }

      let deltaSeconds = reply.time
        .split(";")
        .reduce((r, a, i) => r + a * multipliers[i], 0);
      let time = new Date(post.time.getTime() + deltaSeconds * 1000);
      let readableTime = reply.time
        .split(";")
        .map((e, i) => e + acronyms[i])
        .join(" ");

      return {
        ...reply,
        time: time.toUTCString(),
        readableTime,
      };
    })
    .flat();

  post.renderables = [
    {
      author: post.author,
      content: post.content,
      time: post.time.toUTCString(),
      readableTime: post.time.toUTCString(),
    },
    ...post.replies,
  ];

  post.renderables.forEach((renderable) => {
    const matches = [...renderable.content.matchAll(corrupted)];
    matches.forEach(
      (match) =>
        (renderable.content = renderable.content.replace(
          match[0],
          corruptedString(match[1], match[1])
        ))
    );
  });

  return {
    title: post.title,
    renderables: post.renderables,
  };
};

export default parsePost;
