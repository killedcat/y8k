import parsePost from "./post/parsePost";

const resultsPerPage = 10;

const context = require.context("./../assets/posts/", false, /\.ya?ml$/);
const relevant = context.keys().filter((e) => e.startsWith("."));
const content = relevant.map(context);
const posts = content.map((e, i) => ({
  ...e.default,
  time: new Date(relevant[i].slice(2, -5) * 1000),
}));

const getPosts = (page) =>
  posts.slice(page * resultsPerPage, (page + 1) * resultsPerPage);

const maxPages = Math.floor(posts.length / resultsPerPage);

const getPost = (id) =>
  parsePost({
    ...require(`./../assets/posts/${id}.yaml`).default,
    time: new Date(id * 1000),
  });

export { getPosts, getPost, maxPages };
