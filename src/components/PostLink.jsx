import React from "react";

import {
  PostLinkContainer,
  PostLinkTitle,
  PostLinkAuthor,
  PostLinkTime,
} from "assets/styles/PostLink.styles.jsx";

const PostLink = ({ post }) => (
  <PostLinkContainer
    onClick={() =>
      (window.location.href = Math.floor(post.time.getTime() / 1000))
    }
  >
    <PostLinkTitle>{post.title}</PostLinkTitle>
    <PostLinkAuthor>{post.author}</PostLinkAuthor>
    <PostLinkTime>{post.time.toUTCString()}</PostLinkTime>
  </PostLinkContainer>
);

export default PostLink;
