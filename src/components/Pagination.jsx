import React, { useEffect, useState } from "react";

import {
  PaginationContainer,
  PaginationElementsContainer,
  PaginationInput,
  PaginationArrow,
} from "assets/styles/Pagination.styles.jsx";

const Pagination = ({ page, setPage, maxPage }) => {
  const [intermediate, setIntermediate] = useState(page);
  const validatePage = (page) => Math.max(Math.min(maxPage, parseInt(page)), 0);

  useEffect(() => setIntermediate(page), [page]);

  return (
    <PaginationContainer>
      <PaginationElementsContainer>
        <PaginationArrow
          active={page > 0}
          onClick={() => setPage(validatePage(page - 1))}
        >
          {"<"}
        </PaginationArrow>
        <PaginationInput
          value={intermediate}
          onChange={(e) => {
            const re = /^[0-9\b]+$/;
            if (e.target.value === "" || re.test(e.target.value)) {
              setIntermediate(e.target.value);
            }
          }}
          onKeyDown={(e) =>
            e.key === "Enter" && setPage(validatePage(intermediate))
          }
          onBlur={() => setPage(validatePage(intermediate))}
        />
        <PaginationArrow
          active={page < maxPage}
          onClick={() => setPage(validatePage(page + 1))}
        >
          {">"}
        </PaginationArrow>
      </PaginationElementsContainer>
    </PaginationContainer>
  );
};

export default Pagination;
