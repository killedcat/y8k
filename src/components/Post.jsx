import React, { useState } from "react";
import { useParams } from "react-router-dom";

import ReactMarkdown from "react-markdown";

import {
  PostContainer,
  PostTitle,
  PostRepliesContainer,
  PostReplyContainer,
  PostReplyHeaderContainer,
  PostReplyHeaderAuthor,
  PostReplyHeaderTime,
  PostReplyContent,
  PostBackContainer,
  PostBackLink
} from "assets/styles/Post.styles.jsx";

import { getPost } from "actions/posts.js";

const Post = () => {
  let { id } = useParams();
  const post = getPost(id);

  const [isRenderableTimeReadable, setIsRenderableTimeReadable] = useState(
    post.renderables.map(() => false)
  );

  const toggleReadableTime = (renderableIndex) =>
    setIsRenderableTimeReadable((prevReadables) => [
      ...prevReadables.slice(0, renderableIndex),
      !prevReadables[renderableIndex],
      ...prevReadables.slice(renderableIndex + 1),
    ]);

  return (
    <PostContainer>
      <PostTitle>{post.title}</PostTitle>
      <PostRepliesContainer>
        {post.renderables.map((reply, i) => (
          <PostReplyContainer key={reply.time}>
            <PostReplyHeaderContainer
              onClick={() =>
                (window.location.href = `https://registry.${window.host}/${reply.author}`)
              }
              onMouseEnter={() => toggleReadableTime(i)}
              onMouseLeave={() => toggleReadableTime(i)}
            >
              <PostReplyHeaderAuthor>{reply.author}</PostReplyHeaderAuthor>
              <PostReplyHeaderTime>
                {isRenderableTimeReadable[i] ? reply.readableTime : reply.time}
              </PostReplyHeaderTime>
            </PostReplyHeaderContainer>
            <PostReplyContent>
              <ReactMarkdown>{reply.content}</ReactMarkdown>
            </PostReplyContent>
          </PostReplyContainer>
        ))}
      <PostBackContainer>
        <PostBackLink href={document.referrer}>
          {'<'} Back
        </PostBackLink>
      </PostBackContainer>
      </PostRepliesContainer>
    </PostContainer>
  );
};

export default Post;
