import React from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Header from "components/Header.jsx";
import Posts from "components/Posts.jsx";

const App = () => {
  return (
    <AppContainer>
      <Header />
      <Posts />
    </AppContainer>
  );
};

export default App;
