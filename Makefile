define ENV
HOST=localhost:8000
endef
export ENV

start: .env
	source .env; yarn start & pipenv run start

install: .env

.env:
	echo "$$ENV" > .env && yarn && pipenv install --dev

lint:
	yarn lint && pipenv run lint

format:
	yarn format && pipenv run format

reset:
	rm -rf node_modules/; pipenv --rm; rm -f .env